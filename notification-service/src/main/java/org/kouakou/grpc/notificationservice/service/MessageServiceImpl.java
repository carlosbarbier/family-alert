package org.kouakou.grpc.notificationservice.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.kouakou.grpc.user.Contact;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessageService {
    @Value("${twilio_account_sid}")
    private String ACCOUNT_SID;
    @Value("${twilio_auth_id}")
    private String AUTH_ID;

    @PostConstruct
    private void initialize() {
        Twilio.init(ACCOUNT_SID, AUTH_ID);
    }

    @Override
    public void sendMessage(String from, String longitude,String latitude, List<Contact> contacts) {
        sendToMultipleContacts(from, longitude,latitude,contacts);
    }

    private void sendToMultipleContacts(String from,String longitude,String latitude, List<Contact> contacts) {
        List<String> numbers = contacts.stream().map(Contact::getNumber).collect(Collectors.toList());
        for (String number : numbers) {
            try{
                Message.creator(new PhoneNumber(number), new PhoneNumber(from), "Hey Could you help me I am located at "+longitude+"," +latitude)
                        .create();
            }catch (Exception e){
                System.out.println(e.getMessage());
            }

        }
    }


}
