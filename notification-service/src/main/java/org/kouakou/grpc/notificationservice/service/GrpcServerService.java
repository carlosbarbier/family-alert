package org.kouakou.grpc.notificationservice.service;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.client.inject.GrpcClient;
import net.devh.boot.grpc.server.service.GrpcService;
import org.kouakou.grpc.message.MessageRequest;
import org.kouakou.grpc.message.MessageResponse;
import org.kouakou.grpc.message.MessageServiceGrpc;
import org.kouakou.grpc.user.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@GrpcService
public class GrpcServerService extends MessageServiceGrpc.MessageServiceImplBase {

    @GrpcClient("local-grpc-server")
    private UserServiceGrpc.UserServiceBlockingStub userStub;
    @Autowired
    private MessageService messageService;

    @Override
    public void sendMessageResponse(MessageRequest request, StreamObserver<MessageResponse> responseObserver) {
        List<Contact> contactsList = userStub.getUserContactList(request.getUser()).getContactsList();
        if (!contactsList.isEmpty()) {
            messageService.sendMessage("+13177583581",request.getLongitude(),request.getLatitude(), contactsList);
            MessageResponse messageResponse = MessageResponse
                    .newBuilder()
                    .setStatus(Status.SENT)
                    .build();
            responseObserver.onNext(messageResponse);
            responseObserver.onCompleted();
        } else {
            responseObserver.onError(new StatusRuntimeException(io.grpc.Status.NOT_FOUND.withDescription("User not found")));
        }
    }

}
