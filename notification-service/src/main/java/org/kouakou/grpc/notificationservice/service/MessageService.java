package org.kouakou.grpc.notificationservice.service;


import org.kouakou.grpc.user.Contact;

import java.util.List;

public interface MessageService {
    void sendMessage(String from, String longitude,String latitude,List<Contact> contacts);
}
