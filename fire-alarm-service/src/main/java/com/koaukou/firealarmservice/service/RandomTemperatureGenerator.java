package com.koaukou.firealarmservice.service;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Component
public class RandomTemperatureGenerator {
    private static final List<Integer> temperatures =
            Arrays.asList(18, 19, 20, 25, 30, 40, 11, 24, 32, 42, 80);

    public int getRandoms() {
        return temperatures.get(new Random().nextInt(temperatures.size()));
    }
}
