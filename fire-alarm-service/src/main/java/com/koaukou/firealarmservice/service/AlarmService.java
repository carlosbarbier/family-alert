package com.koaukou.firealarmservice.service;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.kouakou.grpc.alarm.AlarmServiceGrpc;
import org.kouakou.grpc.alarm.Empty;
import org.kouakou.grpc.alarm.TemperatureResponse;
import org.springframework.beans.factory.annotation.Autowired;

@GrpcService
public class AlarmService extends AlarmServiceGrpc.AlarmServiceImplBase {
    @Autowired
    RandomTemperatureGenerator generator;

    @Override
    public void getTemperature(Empty request, StreamObserver<TemperatureResponse> responseObserver) {
        try {
            TemperatureResponse temperatureResponse = TemperatureResponse.newBuilder()
                    .setTemperature(generator.getRandoms())
                    .build();

            responseObserver.onNext(temperatureResponse);
            responseObserver.onCompleted();

        } catch (Exception exception) {
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL.withDescription("An error occurred ")));
        }
    }
//
}
