package com.koaukou.firealarmservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FireAlarmServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FireAlarmServiceApplication.class, args);
    }

}
