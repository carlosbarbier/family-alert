const PROTO_PATH = __dirname + '/proto/user.proto';
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

let packageDefinition = protoLoader.loadSync(PROTO_PATH,);
let user_proto= grpc.loadPackageDefinition(packageDefinition).user;

const user_client = new user_proto.UserService('127.0.0.1:9898', grpc.credentials.createInsecure());

module.exports = user_client;

