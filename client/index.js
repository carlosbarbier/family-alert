const alarm_client = require("./alarm_client");
const user_client = require("./user_client");
const message_client = require("./message_client");
const cors = require('cors');
const express = require("express");
const bodyParser = require("body-parser");

//express setup
const port = process.env.PORT || 9895;
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors())
const router = express.Router();




//ROUTES

//get temperature
router.get('/temperature', function (req, res) {
    const call = alarm_client.getTemperature(null)
    call.on('data', function (response) {
        res.json(response);
    });
});

//USER
router.post('/register', function (req, res) {
    const user = {
        firstName: req.body.first_name,
        lastName: req.body.last_name,
        number: req.body.number,
        password: req.body.password
    }
    user_client.registerUser(user, (error, user) => {
        if (!error) {
            res.send(user)
        } else {
            res.send({error:"An error occurred during the request please try later "})
        }
    })
});

router.post('/login', function (req, res) {
    const data = {
        number: req.body.number,
        password: req.body.password
    }
    user_client.UserLogin(data, (error, user) => {
        if (!error) {
            res.send(user)
        } else {
            res.send({error:"An error occurred during the request please try later "})
        }
    })
});

router.post('/contacts', function (req, res) {
    const data = {
        name: req.body.name,
        number: req.body.number,
        userId:req.body.user_id
    }

    user_client.addNewContact(data, (error, user) => {
        console.log(user)
        if (!error) {
            res.send(user)
        } else {
            res.send({error:"An error occurred during the request please try later "})
        }
    })
});

router.post('/contacts/delete', function (req, res) {
   const data={
       number:req.body.number
   }
    user_client.deleteContact(data, (error, data) => {
        console.log(data)
        if (!error) {
            res.send(data)
        } else {
            res.send({error:"An error occurred during the request please try later "})
        }
    })
});

router.post('/change/number', function (req, res) {
    const data={
        newNumber:req.body.new_number,
        userId:req.body.user_id
    }

    user_client.updateContact(data, (error, data) => {
        if (!error) {
            res.send(data)
        } else {
            res.send({error:"An error occurred during the request please try later "})
        }
    })
});

router.get('/contacts', function (req, res) {

    user_client.getUserContactList({id:parseInt(req.query.id)}, (error, contacts) => {
        if (!error) {
            res.send(contacts)
        } else {
            res.send({error:"An error occurred during the request please try later "})
        }
    })
});

router.post('/notification', function (req, res) {
   const data= {
        user: {
        id: parseInt(req.body.user_id)
    },
        longitude: req.body.longitude,
        latitude:req.body.latitude
    }
    message_client.sendMessageResponse(data, (error, status) => {
        if (!error) {
            res.send(status)
        } else {
            res.send({error:"An error occurred during the request please try later "})
        }
    })
});



app.use('/api', router);


app.listen(port, (err) => {
    if (err) console.error('Server down')
    console.log('server running ' + port);
});
