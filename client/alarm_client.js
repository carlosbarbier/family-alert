const PROTO_PATH = __dirname + '/proto/alarm.proto';
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

let packageDefinition = protoLoader.loadSync(PROTO_PATH,);
let alarm_proto = grpc.loadPackageDefinition(packageDefinition).alarm;

const alarm_client = new alarm_proto.AlarmService('127.0.0.1:9899', grpc.credentials.createInsecure());

module.exports = alarm_client;
