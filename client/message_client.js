const PROTO_PATH = __dirname + '/proto/message.proto';
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

let packageDefinition = protoLoader.loadSync(PROTO_PATH,);
let message_proto = grpc.loadPackageDefinition(packageDefinition).user;

const message_client = new message_proto.MessageService('127.0.0.1:9897', grpc.credentials.createInsecure());

module.exports = message_client;
