import cookie from "js-cookie";

export const setCookie = (key, value) => {
    if (process.browser) {
        cookie.set(key, value, {expires: 1});
    }
};

export const getCookie = (key) => {
    if (process.browser) {
        return cookie.get(key);
    }
};
export const removeCookie = (key) => {
    if (process.browser) {
        cookie.remove(key);
    }
};

export const getFromLocalstorage = (key, value) => {
    if (process.browser) {
        localStorage.setItem(key, JSON.stringify(value));
    }
};
export const storeToLocalstorage = (key, value) => {
    if (process.browser) {
        localStorage.setItem(key, JSON.stringify(value));
    }
};
export const removeFromLocalstorage = (key) => {
    if (process.browser) {
        localStorage.removeItem(key);
    }
};


export const getUser = () => {
    if (process.browser) {
        if (cookie.get("user")) {
            return JSON.parse(cookie.get("user"));
        }
    }
};

export const isLogin = () => {
    if (process.browser) {
        return getUser();
    }
};