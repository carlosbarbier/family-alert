const URL = "http://localhost:9895/api";

export const getTemperature = async () => {
    try {
        const response = await fetch(`${URL}/temperature`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "GET",
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const registerNewUser = async (user) => {
    try {
        const response = await fetch(`${URL}/register`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(user),
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const login = async (data) => {
    try {
        const response = await fetch(`${URL}/login`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const addNewContact = async (data) => {
    try {
        const response = await fetch(`${URL}/contacts`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const getContacts = async (id) => {
    try {
        const response = await fetch(`${URL}/contacts?id=${id}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "GET",
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const deleteContact = async (number) => {
    try {
        const response = await fetch(`${URL}/contacts/delete`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(number),
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const updateContact = async (data) => {
    console.log(data)
    try {
        const response = await fetch(`${URL}/change/number`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const notify = async (data) => {
    try {
        const response = await fetch(`${URL}/notification`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};


