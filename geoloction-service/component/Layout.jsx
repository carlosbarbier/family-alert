import React from 'react';

const Layout = ({children}) => {
    return (
        <div className="container my-6">
            <div className="columns is-mobile">
                <div className="column is-half is-offset-one-quarter">
                    <div className="card px-6">
                        <div className="card-content ">
                            {children}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Layout;