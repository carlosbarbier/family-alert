import React, {useEffect, useState} from 'react';
import Layout from "../component/Layout";
import {addNewContact, deleteContact, getContacts, getTemperature, notify, updateContact} from "../action/api-call";
import cookies from "next-cookies";


const My = ({data, user}) => {
    const [showNewContact, setShowNewContact] = useState(true)
    const [showMyContacts, setContacts] = useState(false)
    const [showSetting, setSetting] = useState(false)
    const [longitude, setLongitude] = useState(null)
    const [latitude, setLatitude] = useState(null)
    const [temperature, setTemperature] = useState(0)
    const [danger, setDanger] = useState(0)
    const [status, setStatus] = useState(false)
    const [notification, setNotification] = useState(false)
    const [contacts, setContactsList] = useState(data || [])

    const [state, setState] = useState({
        name: "",
        number: "",
        errors: null,
    });

    const [update, setUpdate] = useState({
        current: "",
        new_number: "",
        errors: null,
        success: false,
    });

    const [loading, setLoading] = useState(false)

    const {name, number} = state;


    useEffect(() => {
        loadData()
        setInterval(loadData, 3000);
    }, []);
    const loadData = async () => {
        try {
            const res = await getTemperature();
            setTemperature(res.temperature)
            if(res.temperature>50){
                setDanger(true)
            }

        } catch (e) {
            console.log(e);
        }
    }

    function handleContact() {
        setShowNewContact(false)
        setContacts(true)
        setSetting(false)
    }

    function handleSetting() {
        setShowNewContact(false)
        setContacts(false)
        setSetting(true)
    }

    function handleNewContact() {
        setShowNewContact(true)
        setContacts(false)
        setSetting(false)
    }


    const handleCreateNewContact = (e) => {
        e.preventDefault();
        setLoading(true)
        addNewContact({name, number, user_id: user.id}).then((data) => {
            if (data.error) {
                setLoading(false)
                setState({...state, errors: data.error})
            } else {
                setState({...state, name: "", number: "", loading: false});
                setLoading(false)
                setContactsList([...contacts, data])
                handleContact()

            }
        });


    };
    const handleChange = (name) => (e) => {
        setState({...state, [name]: e.target.value});
    };

    const handleUpdate = (name) => (e) => {
        setUpdate({...state, [name]: e.target.value});
    };

    const handleAlertFamily =  () => {
        navigator.geolocation.getCurrentPosition(async position => {
            setLatitude(position.coords.latitude)
            setLongitude(position.coords.longitude)
           const result= await notify({longitude,latitude,user_id:user.id})
            if(result.error){
                console.log("error")
            }
            setNotification(true)
            console.log(result)

        })


    }

    const handleDeleteContact = async (e) => {
        const number = e.currentTarget.dataset.number;
        const result = await deleteContact({number})
        if (result.error) {
            console.log(error)
        } else {
            setContactsList([...contacts.filter(contact => contact.number !== number)])
        }
    }

    const handleUpdateContact = async (e) => {
        e.preventDefault();
        setLoading(true)
        updateContact({
            new_number: update.new_number,
            user_id: user.id
        }).then((data) => {
            if (data.error) {
                setLoading(false)
            } else {
                setUpdate({...update, current: "", new_number: ""});
                setLoading(false)
                setStatus(true)
                handleContact()
            }
        });
    }

    function handleStatus() {
        setStatus(false)
    }

    function handleNotification() {
       setNotification(false)
    }

    function handleDanger() {
        setDanger(false)
    }

    return (
        <Layout>

            {status && <div className="notification is-primary is-light">
                <button className="delete" onClick={handleStatus}/>
                Phone number updated
            </div>}

            {notification && <div className="notification is-primary is-light">
                <button className="delete" onClick={handleNotification}/>
                An alert has been to send to your family
            </div>}

            {danger && <div className="notification is-danger is-light">
                <button className="delete" onClick={handleDanger}/>
                We detected an unusual temperature at home
            </div>}

            <div className=" is-flex is-flex-direction-row-reverse my-3">
                <button className="button is-warning is-uppercase has has-text-weight-semibold"
                        onClick={handleAlertFamily}>Alert My Contact
                </button>
                <button className="button mx-3 is-uppercase"
                        ><span>Temperature:</span> <strong
                    className='mx-2'>{temperature}°C</strong>
                </button>
            </div>
            <div className="tabs is-centered">
                <ul>
                    <li className={showNewContact ? 'is-active' : ''} onClick={handleNewContact}><a>Add New Contact</a>
                    </li>
                    <li className={showMyContacts ? 'is-active' : ''} onClick={handleContact}><a>Contact List</a>
                    </li>
                    <li className={showSetting ? 'is-active' : ''} onClick={handleSetting}><a>Settings</a></li>
                </ul>
            </div>

            {showNewContact && (
                <form onSubmit={handleCreateNewContact}>
                    <div className="setting">
                        <div className="control my-3">
                            <input className="input" type="text" placeholder="Name" name="name"
                                   onChange={handleChange('name')}/>
                        </div>
                        <div className="control my-3">
                            <input className="input" type="text" placeholder="New Phone Number" name="number"
                                   onChange={handleChange('number')}/>
                        </div>
                        {loading ? <button className="is-loading  button is-primary">Add new Contact</button>
                            : <button className="button is-primary">Add new Contact</button>}


                    </div>
                </form>


            )}
            {showSetting && (
                <form onSubmit={handleUpdateContact}>
                    <div className="setting">
                        <div className="control my-3">
                            <input className="input" type="text" placeholder="Phone Number" name="current"
                                   onChange={handleUpdate('current')}/>
                        </div>
                        <div className="control my-3">
                            <input className="input" type="text" onChange={handleUpdate('new_number')}
                                   placeholder="New Phone Number" name="new_number"/>
                        </div>
                        {loading ? <button className="is-loading  button is-primary">Change Number</button>
                            : <button className="button is-primary">Change Number</button>}

                    </div>
                </form>


            )}
            {showMyContacts && (
                <div className="contact is-flex is-justify-content-center">
                    <table className="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Phone Number</th>
                            <th style={{textAlign: "center"}}>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {contacts && contacts.map((contact, index) => (
                            <tr key={index}>
                                <th>{contact.name}</th>
                                <th>{contact.number}</th>
                                <th>
                                    <button className="button is-small is-primary is-outlined disabled mx-4">
                                                    <span className="icon is-small">
                                                    <i className="las la-pen-alt"/>
                                                    </span>
                                    </button>
                                    <button data-number={contact.number}
                                            className="button is-danger is-outlined  is-small"
                                            onClick={handleDeleteContact}>
                                                    <span className="icon is-small">
                                                         <i className="las la-trash"/>
                                                     </span>
                                    </button>
                                </th>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>

            )}
        </Layout>
    );
};
My.getInitialProps = async (ctx) => {
    const {user} = cookies(ctx);
    if (user !== undefined) {
        const contacts = await getContacts(user.id)
        return {data: contacts.contacts, user};
    } else {
        ctx.res.writeHead(302, {Location: '/'});
        ctx.res.end();
    }

};

export default My;