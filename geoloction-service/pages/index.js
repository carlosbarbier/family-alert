import React, {useState} from 'react';

import Link from "next/link";
import Layout from "../component/Layout";
import Router from "next/router";
import {login} from "../action/api-call";
import {setCookie, storeToLocalstorage} from "../action/util";
import cookies from "next-cookies";

const Home = () => {
    const [state, setState] = useState({
        password: "",
        number: "",
        errors: null,
    });

    const [loading, setLoading] = useState(false)

    const {password, number, errors} = state;

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true)
        login({password, number}).then((data) => {
            if (data.errors) {
                setLoading(false)
                setState({...state, errors: data.error})
            } else {
                storeToLocalstorage("user", data)
                setCookie("user", data)
                setState({password: "", number: "", loading: false});
                setTimeout(() => {
                    Router.push("/my");
                    setLoading(false)
                }, 3000);
            }
        });


    };
    const handleChange = (name) => (e) => {
        setState({...state, [name]: e.target.value});
    };

    return (
        <Layout>
            {errors && <p className="help is-danger">
                {errors}
            </p>}

            <form onSubmit={handleSubmit}>
                <div className="field">
                    <label className="label">Phone Number</label>
                    <div className="control">
                        <input className="input" type="text" name="number" onChange={handleChange('number')}
                               placeholder="e.g. 0899464470"/>
                    </div>
                </div>

                <div className="field">
                    <label className="label">Password</label>
                    <div className="control">
                        <input className="input" type="password" name="password" onChange={handleChange('password')}
                               placeholder="********"/>
                    </div>
                </div>
                {loading ? <button className="is-loading  button is-primary">Sign in</button>
                    : <button className="button is-primary">Sign in</button>}
                <div className="mt-3"><span className="mt-6">Don't have an account? </span> <span
                    className="is-clickable has-text-link"> <Link href="/register">Register </Link> </span></div>
            </form>
        </Layout>
    )
}

Home.getInitialProps = async (ctx) => {
    const {user} = cookies(ctx);
    if(user){
        ctx.res.writeHead(302, {Location: '/my'});
        ctx.res.end();
    }
    return {};
};

export default Home;