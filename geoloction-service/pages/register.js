import React, {useEffect, useState} from 'react';
import Layout from "../component/Layout";
import {registerNewUser} from "../action/api-call";
import Router from "next/router";

const Register = () => {
    const [state, setState] = useState({
        first_name: "",
        last_name: "",
        password: "",
        number: "",
        message: null,
        success: false,
        errors: null,
    });
    const [loading, setLoading] = useState(false)
    useEffect(() => {
    }, []);

    const {first_name, last_name, password, number, errors, success} = state;

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true)
        registerNewUser({first_name, last_name, password, number}).then((data) => {
            if (data.error) {
                setLoading(false)
            } else {
                setState({
                    name: "",
                    email: "",
                    password: "",
                    loading: false,
                    message: data.message,
                    success: true,
                });
                setTimeout(() => {
                    Router.push("/");
                    setLoading(false)
                }, 3000);
            }
        });


    };
    const handleChange = (name) => (e) => {
        setState({...state, [name]: e.target.value});
    };

    return (
        <Layout>
            <form onSubmit={handleSubmit}>
                <div className="field">
                    <label className="label">First Name</label>
                    <div className="control">
                        <input className="input" type="text" name='first_name' onChange={handleChange('first_name')}
                               placeholder="e.g. Carlos B."/>
                    </div>
                </div>

                <div className="field">
                    <label className="label">Last Name</label>
                    <div className="control">
                        <input className="input" type="text" name='last_name' onChange={handleChange('last_name')}
                               placeholder="e.g. Carlos B."/>
                    </div>
                </div>

                <div className="field">
                    <label className="label">Phone Number</label>
                    <div className="control">
                        <input className="input" type="text" name='number' onChange={handleChange('number')}
                               placeholder="e.g. +353808080808"/>
                    </div>
                </div>

                <div className="field">
                    <label className="label">Password</label>
                    <div className="control">
                        <input className="input" type="password" name='password' onChange={handleChange('password')}
                               placeholder="********"/>
                    </div>
                </div>
                {loading ? <button className="is-loading  button is-primary">Register</button>
                    : <button className="button is-primary">Register</button>}

            </form>
        </Layout>
    );
};

export default Register;