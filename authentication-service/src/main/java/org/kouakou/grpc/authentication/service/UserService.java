package org.kouakou.grpc.authentication.service;


import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.kouakou.grpc.authentication.dto.UserDto;
import org.kouakou.grpc.authentication.entity.ContactEntity;
import org.kouakou.grpc.authentication.entity.UserEntity;
import org.kouakou.grpc.authentication.repository.ContactRepository;
import org.kouakou.grpc.authentication.repository.UserRepository;
import org.kouakou.grpc.authentication.utils.PasswordUtils;
import org.kouakou.grpc.user.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.kouakou.grpc.user.Status.DELETED;

@GrpcService
public class UserService extends UserServiceGrpc.UserServiceImplBase {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ContactRepository contactRepository;


    @Override
    public void userLogin(UserLoginRequest request, StreamObserver<User> responseObserver) {
        final String number = request.getNumber();
        final String password = request.getPassword();

        try {
            final UserEntity userFound = userRepository.authUser(number);

            if (userFound == null) {
                responseObserver.onError(
                        new StatusRuntimeException(Status.NOT_FOUND.withDescription("User not found"))
                );
            } else {
                if (PasswordUtils.match(password, userFound.getPassword())) {
                    responseObserver.onNext(
                            User.newBuilder()
                                    .setId(userFound.getId().intValue())
                                    .setFirstName(userFound.getFirstName())
                                    .setLastName(userFound.getLastName())
                                    .setNumber(userFound.getNumber())
                                    .build()

                    );
                    responseObserver.onCompleted();
                } else {
                    responseObserver.onError(
                            new StatusRuntimeException(Status.ABORTED.withDescription("Bad Request"))
                    );

                }
            }

        } catch (Exception exception) {
            responseObserver.onError(
                    new StatusRuntimeException(Status.INVALID_ARGUMENT.withDescription("Invalid Argument provided"))
            );
        }


    }

    @Override
    public void registerUser(RegisterUserRequest request, StreamObserver<User> responseObserver) {
        final String passwordHashed = PasswordUtils.hash(request.getPassword());
        final UserDto dto = new UserDto(request.getFirstName(), request.getLastName(), request.getNumber(), passwordHashed);
        final UserEntity user = new UserEntity(dto);
        try {
            final UserEntity userSaved = userRepository.save(user);
            responseObserver.onNext(
                    User.newBuilder()
                            .setId(userSaved.getId().intValue())
                            .setFirstName(userSaved.getFirstName())
                            .setLastName(userSaved.getLastName())
                            .setNumber(user.getNumber())
                            .build()

            );
            responseObserver.onCompleted();

        } catch (Exception exception) {
            responseObserver.onError(
                    new StatusRuntimeException(Status.INVALID_ARGUMENT.withDescription("Invalid Argument provided"))
            );
        }
    }

    @Override
    public void findUserById(FindUserRequest request, StreamObserver<User> responseObserver) {
        try {
            Optional<UserEntity> user = userRepository.findById((long) request.getId());
            if (user.isPresent()) {
                responseObserver.onNext(
                        User.newBuilder()
                                .setFirstName(user.get().getFirstName())
                                .setLastName(user.get().getLastName())
                                .setNumber(user.get().getNumber())
                                .build()
                );
                responseObserver.onCompleted();
            } else {
                responseObserver.onError(new StatusRuntimeException(Status.NOT_FOUND.withDescription("User not found")));
            }

        } catch (Exception exception) {
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL.withDescription("Internal server error")));
        }
    }

    @Override
    public void deleteUserById(FindUserRequest request, StreamObserver<UserDelete> responseObserver) {
        try {
            Optional<UserEntity> user = userRepository.findById((long) request.getId());
            if (user.isPresent()) {
                userRepository.deleteById((long) request.getId());
                responseObserver.onNext(
                        UserDelete.newBuilder()
                                .setStatus(DELETED)
                                .build()
                );
                responseObserver.onCompleted();

            } else {
                responseObserver.onError(new StatusRuntimeException(Status.NOT_FOUND.withDescription("User not found")));
            }

        } catch (Exception exception) {
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL.withDescription("Internal server error")));
        }
    }

    @Override
    public void getUserContactList(FindUserRequest request, StreamObserver<UserContact> responseObserver) {
        System.out.println("GOIT call");
        try {
            List<ContactEntity> contactEntities = contactRepository.findAllContactByUserId((long) request.getId());
            Iterable<Contact> friends = contactEntities.stream()
                    .map(contactEntity ->
                            Contact.newBuilder()
                                    .setName(contactEntity.getName())
                                    .setNumber(contactEntity.getNumber()).build())

                    .collect(Collectors.toList());
            responseObserver.onNext(
                    UserContact.newBuilder()
                            .addAllContacts(friends)
                            .build());
            responseObserver.onCompleted();

        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL.withDescription("Internal server error")));

        }
    }


    @Override
    public void deleteContact(DeleteContactRequest request, StreamObserver<DeleteContactResponse> responseObserver) {
        try {
            ContactEntity entity = contactRepository.findContactEntityByNumber(request.getNumber());
            if (entity != null) {
                contactRepository.deleteById(entity.getId());
                responseObserver.onNext(
                        DeleteContactResponse.newBuilder()
                                .setStatus(DELETED)
                                .build());
                responseObserver.onCompleted();
            } else {
                responseObserver.onError(new StatusRuntimeException(Status.ABORTED.withDescription("Contact not found")));
            }


        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL.withDescription("Internal server error")));
        }
    }

    @Override
    public void updateContact(UpdateContactRequest request, StreamObserver<Empty> responseObserver) {
        try {
            Optional<UserEntity> userEntity = userRepository.findById(Long.parseLong(request.getUserId()));
            if (userEntity.isPresent()) {
                userEntity.get().setNumber(request.getNewNumber());
                userRepository.save(userEntity.get());
                responseObserver.onNext(Empty.newBuilder().build());
                responseObserver.onCompleted();
            } else {
                responseObserver.onError(new StatusRuntimeException(Status.NOT_FOUND.withDescription("We can not find this number")));
            }


        } catch (Exception exception) {
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL.withDescription("Internal server error")));
        }
    }

    @Override
    public void addNewContact(AddNewContactRequest request, StreamObserver<AddNewContactResponse> responseObserver) {
        try {
            ContactEntity newContactEntity = new ContactEntity();
            newContactEntity.setNumber(request.getNumber());
            newContactEntity.setUserId(Long.parseLong(request.getUserId()));
            newContactEntity.setName(request.getName());
            ContactEntity contactSaved = contactRepository.save(newContactEntity);
            responseObserver.onNext(
                    AddNewContactResponse.newBuilder()
                            .setNumber(contactSaved.getNumber())
                            .setName(contactSaved.getName())
                            .build());
            responseObserver.onCompleted();

        } catch (Exception exception) {
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL.withDescription("Internal server error")));
        }


    }

}
