package org.kouakou.grpc.authentication.utils;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Component;

public class PasswordUtils {

    public static String hash(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(15));
    }

    public static boolean match(String password, String hashed) {
        return BCrypt.checkpw(password, hashed);
    }
}
