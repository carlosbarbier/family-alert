package org.kouakou.grpc.authentication.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserContactDto {
    private UserDto userDto;
    List<String>contacts;
}
