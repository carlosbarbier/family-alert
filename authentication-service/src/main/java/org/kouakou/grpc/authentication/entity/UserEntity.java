package org.kouakou.grpc.authentication.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.kouakou.grpc.authentication.dto.UserDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Table(name = "user")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    private String firstName;
    private String lastName;
    private String number;
    private String password;

    public UserEntity(UserDto dto) {
        firstName = dto.getFirstname();
        lastName = dto.getLastName();
        number = dto.getNumber();
        password = dto.getPassword();
    }
}
