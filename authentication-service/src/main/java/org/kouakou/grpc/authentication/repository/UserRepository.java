package org.kouakou.grpc.authentication.repository;

import org.jetbrains.annotations.NotNull;
import org.kouakou.grpc.authentication.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    @NotNull
    @Override
    Optional<UserEntity> findById(@NotNull Long id);

    @Query("Select u from UserEntity u where u.number = :number")
    UserEntity authUser(@Param("number") String number);
}
