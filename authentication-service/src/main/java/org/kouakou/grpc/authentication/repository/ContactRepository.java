package org.kouakou.grpc.authentication.repository;

import org.jetbrains.annotations.NotNull;
import org.kouakou.grpc.authentication.entity.ContactEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<ContactEntity, Long> {

    @Query("select c from ContactEntity c where c.userId = :user_id")
    List<ContactEntity> findAllContactByUserId(@NotNull @Param("user_id") Long user_id);


   ContactEntity findContactEntityByNumber(@NotNull String number);

    @Query("select c from ContactEntity c where c.userId = :user_id and  c.number= :number ")
    Optional<ContactEntity> getContact(@NotNull @Param("user_id") Long user_id, @NotNull @Param("number") String number);

}
